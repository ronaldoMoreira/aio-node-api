//=============================================================================================//
// Projeto    : aio
// Modulo     : aioLm
// Responsavel: ronaldo@on-sac.com
// Data       : 01/03/2018
// Objetivo   : Modulo de integração com LogicMonitor
//============================================================================================//
var config          = require('./.aio/aio-node-config.js'); 
var request         = require('request');
const { exec }      = require('child_process');
const moment        = require('moment');
const logTimeFormat = 'D MMM HH:mm:ss - [[log]] ';

var logicMonitor    = {};

if (process.argv.length < 3) {
    console.log("Usage: " + __filename + " SOME_PARAM");
    process.exit(-1);
}

console.log('aioLm: '+moment().format(logTimeFormat) + ' Parametros:', process.argv[2]);

var resp = JSON.parse(process.argv[2]);

var array = Object.keys(resp.query);
array.forEach(function (key) {
   logicMonitor[key] = resp.query[key]; 
});

var array = Object.keys(resp.body);
array.forEach(function (key) {
   logicMonitor[key] = resp.body[key];
});

var options_auth = { 
                   method: 'POST',
                   url : config.host+'/api/authenticate',
                   headers: 
                          { 'Cache-Control': 'no-cache',
                            'Content-Type' : 'application/x-www-form-urlencoded',
                            "Accept"       : "application/json" },
                            form: 
                                 { name: config.name, password: config.password }
                   };

var interface = config.modules[1].interface;
var versao    = config.modules[1].versao;
var nivel     = versao.split('-')[0];
var ambiente  = config.modules[1].ambiente;

request(options_auth, function (error, response, body) {
   if (error) {
      console.log(error);
      console.log('Processamento concluído com ERRO');
      process.exit(-1);
   }
   var jbody = JSON.parse(body);
   var options_upload = { method: 'POST',
                             url: config.host+'/api/event',
                         headers: { 'Cache-Control': 'no-cache',
                                    'Content-Type' : 'application/x-www-form-urlencoded',
                                    'Accept'       : 'application/json' },
                            form: { token          : jbody.token,
                                    origem         : 'logicMonitor',
                                    modulo         : 'aioLm', 
                                    interface      : interface, 
                                    versao         : versao,
                                    event          : logicMonitor } 
                        };
   request(options_upload, function (error, response, body) {
      if (error) {
         console.log(error);
         console.log('Processamento concluído com ERRO');
         process.exit(-1);
      }
      var arqbody = JSON.parse(body);
      console.log('aioLm: '+moment().format(logTimeFormat) + ' ID Catalogo         : '+arqbody.catalogo._id);
      console.log('aioLm: '+moment().format(logTimeFormat) + ' Interface           : '+arqbody.catalogo.interface);
      console.log('aioLm: '+moment().format(logTimeFormat) + ' Versao              : '+arqbody.catalogo.versao);
   });
});   
