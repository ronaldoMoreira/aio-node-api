//=============================================================================================//
// Projeto    : aio
// Modulo     : aioJira
// Responsavel: ronaldo@on-sac.com
// Data       : 01/03/2018
// Objetivo   : Modulo de integração com JIRA
//============================================================================================//
var config          = require('./.aio/aio-node-config.js'); 
var request         = require('request');
const { fork }      = require('child_process');
const moment        = require('moment');
const logTimeFormat = 'D MMM HH:mm:ss - [[log]] ';

if (process.argv.length < 3) {
    console.log("Usage: " + __filename + " SOME_PARAM");
    process.exit(-1);
}

var resp = JSON.parse(process.argv[2]);
var issue = JSON.parse(resp.body.issue);
 
console.log('aioJira: '+moment().format(logTimeFormat) + ' Parametros: ', resp);

var jira = { project         : resp.query.project,
             issueId         : resp.query.issue,
             issueKey        : issue.key,
             userKey         : resp.query.user_key,
             statusId        : issue.fields.status.statusCategory.id,
             statusColorName : issue.fields.status.statusCategory.colorName,
             statusName      : issue.fields.status.statusCategory.name
           };

console.log('aioJira: '+moment().format(logTimeFormat) + ' jira: ', jira);

var options_auth = { 
                   method: 'POST',
                   url : config.host+'/api/authenticate',
                   headers: 
                          { 'Cache-Control': 'no-cache',
                            'Content-Type' : 'application/x-www-form-urlencoded',
                            "Accept"       : "application/json" },
                            form: 
                                 { name: config.name, password: config.password }
                   };

var interface = config.modules[2].interface;
var versao    = config.modules[2].versao;
var nivel     = versao.split('-')[0];
var ambiente  = config.modules[2].ambiente;

request(options_auth, function (error, response, body) {
   if (error) {
      console.log(error);
      console.log('Processamento concluído com ERRO');
      process.exit(-1);
   }
   var jbody = JSON.parse(body);

   var options_upload = { method: 'POST',
                             url: config.host+'/api/event',
                         headers: { 'Cache-Control': 'no-cache',
                                    'Content-Type' : 'application/x-www-form-urlencoded',
                                    'Accept'       : 'application/json' },
                            form: { token          : jbody.token,
                                    origem         : 'jira',
                                    modulo         : 'aioJira',
                                    interface      : interface,
                                    versao         : versao,
                                    event          : JSON.stringify(jira) }
                        };

   request(options_upload, function (error, response, body) {
      if (error) {
         console.log(error);
         console.log('Processamento concluído com ERRO');
         process.exit(-1);
      }
      var arqbody = JSON.parse(body);
      console.log('aioJira: '+moment().format(logTimeFormat) + 'ID Catalogo         : '+arqbody.catalogo._id);
      console.log('aioJira: '+moment().format(logTimeFormat) + 'Interface           : '+arqbody.catalogo.interface);
      console.log('aioJira: '+moment().format(logTimeFormat) + 'Versao              : '+arqbody.catalogo.versao);
      if (jira.statusName == 'DONE') {
         if ( arqbody.incidente ) {
            if (arqbody.incidente.origem == 'controlm' ) {
               fork('aioEm.js', [JSON.stringify({ alertId : arqbody.incidente.description.alertId, alertStatus: 'handle' })]);
            }
         }
      } 
   });
});   
