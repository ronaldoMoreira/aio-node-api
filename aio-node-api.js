//=============================================================================================//
// Projeto    : aio
// Modulo     : aioApi
// Responsavel: ronaldo@on-sac.com
// Data       : 01/03/2018
// Objetivo   : Modulo de integração
//============================================================================================//
var express 	    = require('express');
var fse             = require('fs-extra');
var app             = express();
var bodyParser      = require('body-parser');
var morgan          = require('morgan');
const { parse }     = require('querystring');
var jwt             = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config          = require('./.aio/aio-node-config.js');
var formidable      = require('formidable');
const { fork }      = require('child_process');
const moment        = require('moment');
const logTimeFormat = 'D MMM HH:mm:ss - [[log]] ';
var request         = require('request');
var path            = require('path');

// =================================================================
// configuration ===================================================
// =================================================================
var port = config.apiPort || 8092;
var hostname = process.env.HOSTNAME;

app.set('superSecret', config.secret); // secret variable

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('dev'));

// =================================================================
// routes ==========================================================
// =================================================================

// basic route (http://localhost:8082)
app.get('/', function(req, res) {
	res.send('Oi eu sou o AIO, o endereço da minha API é : http://'+hostname+':'+port+'/api');
});

// ---------------------------------------------------------
// get an instance of the router for api routes
// ---------------------------------------------------------
var apiRoutes = express.Router(); 

// ---------------------------------------------------------
// authentication (no middleware necessary since this isnt authenticated)
// ---------------------------------------------------------
// http://localhost:8080/api/authenticate
apiRoutes.post('/authenticate', function(req, res) {
	// find the user
	User.findOne({
		name: req.body.name
	}, function(err, user) {
		if (err) console.log(err);

		if (!user) {
			res.json({ success: false, message: 'Oi eu sou o AIO, a sua tentativa de autenticação falou. Usuário informado não existe.' });
		} else if (user) {
			// check if password matches
                        if (!user.validPassword(req.body.password)){
			   res.json({ success: false, message: 'Oi eu sou o AIO, a sua tentativa de autenticação falhou. A senha está errada.' });
			} else {
				// if user is found and password is right
				// create a token
				var payload = {
					admin: user.admin	
				}
				var token = jwt.sign(payload, app.get('superSecret'), {
					expiresIn: 86400 // expires in 24 hours
				});
				res.json({
					success: true,
					message: 'Oi eu sou o AIO, segue o seu Token!',
					token: token
				});
			}		
		}
	});
});


// ---------------------------------------------------------
// route middleware to authenticate and check token
// ---------------------------------------------------------
apiRoutes.use(function(req, res, next) {
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
	if (token) {
		jwt.verify(token, app.get('superSecret'), function(err, decoded) {			
			if (err) {
				return res.json({ success: false, message: 'Oi eu sou o AIO, não consegui validar o Token informado.' });		
			} else {
				req.decoded = decoded;	
				next();
			}
		});
	} else {
           next();
	}
});
// ---------------------------------------------------------
// authenticated routes
// ---------------------------------------------------------
apiRoutes.get('/', function(req, res) {
	res.json({ message: 'Oi eu sou o AIO, Seja bem-vindo na minha API!' });
});

apiRoutes.get('/check', function(req, res) {
	res.json(req.decoded);
});

apiRoutes.post('/jira', function(req, res, next) {
  var reqString = JSON.stringify(req.body);
  var reqJson = JSON.parse(reqString);
  var issue = JSON.parse(reqJson.issue);
  console.log(issue);
  var issueChange = { project  : req.query.project,
                      issueId  : req.query.issue,
                      issueKey : issue.key,
                      userKey  : req.query.user_key,
                      status   : { id        : issue.fields.status.statusCategory.id,
                                   colorName : issue.fields.status.statusCategory.colorName,
                                   name      : issue.fields.status.statusCategory.name }
                     };

   console.log('aio-node-api: '+moment().format(logTimeFormat) + ' JIRA:', JSON.stringify(issueChange));
   var resp = {};
   if (req.query) {
      var queryString = JSON.stringify(req.query);
      var queryJson = JSON.parse(queryString);
      if (queryJson !== {}) {
         resp['query'] = queryJson;
      }
   }
   if (req.body) {
      var reqString = JSON.stringify(req.body);
      var reqJson = JSON.parse(reqString);
      if (reqJson !== {}) {
         resp['body'] = reqJson;
      }
   }
   console.log(JSON.stringify(resp));
   fork('aioJira.js', [JSON.stringify(resp)]);
   res.json({ success: true, message: 'OK' });
});

apiRoutes.post('/lm', function(req, res, next) {
   console.log('aio-node-api: '+moment().format(logTimeFormat) + ' LM:', JSON.stringify(req.body));
   var resp = {};
   if (req.query) {
      var queryString = JSON.stringify(req.query);
      var queryJson = JSON.parse(queryString);
      if (queryJson !== {}) {
         resp['query'] = queryJson;
      }
   }
   if (req.body) {
      var reqString = JSON.stringify(req.body);
      var reqJson = JSON.parse(reqString);
      if (reqJson !== {}) {
         resp['body'] = reqJson;
      }
   }
   console.log(JSON.stringify(resp));
   fork('aioLm.js', [JSON.stringify(resp)]);
   res.json({ success: true, message: 'OK' }); 
});

app.use('/api', apiRoutes);

// =================================================================
// start the server ================================================
// =================================================================
app.listen(port);
console.log('aio-node-api: '+moment().format(logTimeFormat) + ' http://'+hostname+':'+port+'/api/');
