var modules = [];

modules[0] = {module:'aioCtm.js',key:'',mib:'',interface:'Api',ambiente:'em900',versao:'9000-500',filtro:'' };
modules[1] = {module:'aioLm.js',key:'',mib:'',interface:'Api',ambiente:'cloud',versao:'0000-115',filtro:'' };
modules[2] = {module:'aioJira.js',key:'',mib:'',interface:'WebHook',ambiente:'cloud',versao:'0000-330',filtro:'' };
modules[3] = {module:'aioEm.js',key:'',mib:'',interface:'Cli',ambiente:'em900',versao:'9000-500',filtro:'' };

module.exports = {
  'secret'    : 'ilovescotchyscotch',
  'host'      : 'http://s01apl678.sulamerica.br:8092',
  'name'      : 'aioapi',
  'password'  : '$2a$08$BulSjpvhleolfr.lt.Dc5OyO/2wVwK46jFCNWjpDy/hb095u2LhLe',
  'snmpPort'  : 1162,
  'apiPort'   : 8080,
  'modules'   : modules
};
